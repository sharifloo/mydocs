# MINIO 
MinIO is high performance destributed object storage server, designed for larg scale data infrastracture such as unstactured data(Image , videos ,sounds), logfiles , time series data 
it written by Golang

MinIO Honors :   
 - github stars : 16.2k
 - Docker pull : 219.3M


types of MinIO:  
1. MinIO server 
2. MinIO client 
3. MinIO SDK 

contain HTTP method PUT , GET 
remove by DELETE 

You need four items in order to connect to MinIO object storage server.

1. Params : Description
2. endpoint: URL to object storage service.
3. access_key: Access key is like user ID that uniquely identifies your account.
4. secret_key: Secret key is the password to your account.
5. secure: Set this value to 'True' to enable secure (HTTPS) access.

## How to install 
At first you have to install minio server on your system. 
- method 1 (stable docker image ): 
```bash 
docker run -p 9000:9000 \
  --name minio1 \
  -v /mnt/data:/data \
  -e "MINIO_ACCESS_KEY=AKIAIOSFODNN7EXAMPLE" \
  -e "MINIO_SECRET_KEY=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY" \
  minio/minio server /data
```
- method 2 (Binary Download): 
```bash
wget https://dl.min.io/server/minio/release/linux-amd64/minio
chmod +x minio
./minio server /data
```
### testing installtion 
MinIO Server comes with an embedded web based object browser. Point your web browser to http://127.0.0.1:9000 to ensure your server has started successfully.


----
## Run MinIO gateway with Disk cache
Disk caching feature here refers to the use of caching disks to store content closer to the tenants. For instance, if you access an object from a lets say gateway azure setup and download the object that gets cached, each subsequent request on the object gets served directly from the cache drives until it expires. This feature allows MinIO users to have

- Object to be delivered with the best possible performance.
- Dramatic improvements for time to first byte for any object.


Disk caching can be enabled by setting the *cache* environment variables for MinIO gateway .   
**Example 1:**   Start MinIO gateway to s3 with edge caching enabled on '/mnt/drive1', '/mnt/drive2' and '/mnt/export1 ... /mnt/export24',
     exclude all objects under 'mybucket', exclude all objects with '.pdf' as extension. Cache only those objects accessed atleast 3 times. Garbage collection triggers in at high water mark (i.e. cache disk usage reaches 90% of cache quota) or at 72% and evicts oldest objects by access time until low watermark is reached ( 70% of cache quota) , i.e. 63% of disk usage.


```bash
export MINIO_CACHE_DRIVES="/mnt/drive1,/mnt/drive2,/mnt/export{1..24}"
export MINIO_CACHE_EXCLUDE="mybucket/*,*.pdf"
export MINIO_CACHE_QUOTA=80
export MINIO_CACHE_AFTER=3
export MINIO_CACHE_WATERMARK_LOW=70
export MINIO_CACHE_WATERMARK_HIGH=90
minio gateway s3
```
## Run MinIO gateway with cache on Docker Container
**Example 2:** Cache drives need to have strictatime or relatime enabled for disk caching feature. In this example, mount the xfs file system on /mnt/cache with strictatime or relatime enabled.    

```bash
truncate -s 4G /tmp/data
mkfs.xfs /tmp/data     # build xfs filesystem on /tmp/data
sudo mkdir /mnt/cache  # create mount dir
sudo mount -o relatime /tmp/data /mnt/cache # mount xfs on /mnt/cache with atime.
docker pull minio/minio

docker run --net=host -e MINIO_ACCESS_KEY={s3-access-key} -e MINIO_SECRET_KEY={s3-secret-key} -e MINIO_CACHE_DRIVES=/cache -e MINIO_CACHE_QUOTA=99 -e MINIO_CACHE_AFTER=0 -e MINIO_CACHE_WATERMARK_LOW=90 -e MINIO_CACHE_WATERMARK_HIGH=95  -v /mnt/cache:/cache  minio/minio:latest gateway s3
```

---- 
## How to use
### what is mc 
**mc(MinIO Client)** provides a modern alternative to UNIX commands like ls, cat, cp, mirror, diff etc. this service help you to access MinIO Server.
you have two method for installing mc : 
1. Docker container 
```bash
docker pull minio/mc
docker run minio/mc ls play
```
2. Binary Download
```bash
wget https://dl.min.io/client/mc/release/linux-amd64/mc
chmod +x mc
./mc --help
```
### test your installation 
Example1 : List all buckets from https://play.min.io
```bash 
mc ls play
[2016-03-22 19:47:48 PDT]     0B my-bucketname/
[2016-03-22 22:01:07 PDT]     0B mytestbucket/
[2016-03-22 20:04:39 PDT]     0B mybucketname/
[2016-01-28 17:23:11 PST]     0B newbucket/
[2016-03-20 09:08:36 PDT]     0B s3git-test/
```
Example2 : **mb** command creates a new bucket.
```bash 
mc mb play/mybucket
Bucket created successfully `play/mybucket`.
```
Example3 : **cp** command copies data from one or more sources to a target.
```bash 
mc cp myobject.txt play/mybucket
myobject.txt:    14 B / 14 B  ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓  100.00 % 41 B/s 0
```

----
## python usage 

### create client and make bucket   

For first try you have to **makes a bucket** on the server and then uploads a file to the bucket.
```python
# Import MinIO library.
from minio import Minio
from minio.error import (ResponseError, BucketAlreadyOwnedByYou,
                         BucketAlreadyExists)

# Initialize minioClient with an endpoint and access/secret keys.
minioClient = Minio('play.min.io',
                    access_key='Q3AM3UQ867SPQQA43P2F',
                    secret_key='zuf+tfteSlswRu7BJ86wekitnifILbZam1KYY3TG',
                    secure=True)

# Make a bucket with the make_bucket API call.
try:
       minioClient.make_bucket("maylogs", location="us-east-1")
except BucketAlreadyOwnedByYou as err:
       pass
except BucketAlreadyExists as err:
       pass
except ResponseError as err:
       raise

# Put an object 'pumaserver_debug.log' with contents from 'pumaserver_debug.log'.
try:
       minioClient.fput_object('maylogs', 'pumaserver_debug.log', '/tmp/pumaserver_debug.log')
except ResponseError as err:
       print(err)

```
